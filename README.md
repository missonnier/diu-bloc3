# Projet DIU Bloc 3 : Utiliser un écran LCD pour afficher des coordonnées GPS 
### Julien Donet & Fabrice Missonnier

## Consignes pour accéder au projet et à la fiche récapitulative
Cloner le projet GitLab avec la commande
`git clone https://gitlab.com/missonnier/diu-bloc3.git`

## Contenu de ce projet GitLab

Nous avons travaillé en binôme sur le projet et nous nous sommes scindé le travail en fonction du matériel que nous avions chacun à la maison.

Le projet permet 
- d'utiliser un écran LCD relié à un Raspberry et d'y afficher des informations
- de recevoir des coordonnées GPS à partir d'un capteur relié à un microcontrôleur Arduino
- de connecter l'Arduino au Raspberry et de contrôler l'Arduino à partir d'une raspbian en mode *core* via le port série

**Le travail est détaillé dans le répertoire `fiche` du projet cloné.** 

Le travail comprend 4 fiches :
- la fiche professeur qui détaille le projet et sa mise en oeuvre
- 3 fiches élèves qui fournissent une progression pédagogique sur les thème de l'Arduino, du Raspberry et de la connexion entre les deux

Les fiches au format `ipynb`, lisibles sous Jupyter. Le répertoire `c-arduino` contient le code source C permettant la récupération des informations émises par le capteur GPS via l'Arduino. Le code source Python récupérant les données et les affichant sur l'écran LCD avec le Raspberry se trouve dans le répertoire `python-rasp`.

Nous avons aussi produit une petite interface Python-Flask qui permet de récupérer les coordonnées du GPS et les affiche sur une page web et sur l'écran LCD.

![Alt](images/montageglobalannote.jpg "Montage global")
