import pynmea2

class traiteGPS():
    def __init__ (self, lequel):
        with open('gpscoord.txt') as f: lines = [line.rstrip('\n') for line in f]

        for line in lines:
            #on récupère 10 lignes avec le dd, on garde la première avec $GPRMC
            if (line.__contains__(lequel)):
                self.lalignecomplete = line
                break

        self.msg = pynmea2.parse(line)
        print(self.msg)
	
    def getHeure(self):
        return self.msg.timestamp

    def getLatitude(self):
        return self.msg.lat
        #'1929.045'

    def getLatitudeDiv100(self):
        return str(float(self.msg.lat)/100)
        #'1929.045'

    def getLatitudeDir(self):
        return self.msg.lat_dir
        #'S'

    def getLongitude(self):
        return self.msg.lon
        #'02410.506'

    def getLongitudeDiv100(self):
        return str(float(self.msg.lon)/100)
        #'02410.506'

    def getLongitudeDir(self):
        return self.msg.lon_dir
        #'E'

    def getGPSQual(self):
        return self.msg.gps_qual
        #'1'

    def getNumSats(self):
        return self.msg.num_sats
        #'04'

    def getHorizDil(self):
        return self.msg.horizontal_dil
        #'2.6'

    def getAltitude(self):
        return self.msg.altitude
        #100.0

    def getAltitudeUnit(self):
        return self.msg.altitude_units
        #'M'

    def getGeoSep(self):
        return self.msg.geo_sep
        #'-33.9'

    def getGeoSepUnit(self):
        return self.msg.geo_sep_units
        #'M'

    def getAgeGPSData(self):
        return self.msg.age_gps_data
        #''

    def getStationId(self):
        return self.msg.ref_station_id
        #'0000'

    def getLigne(self):
        return self.lalignecomplete
        #$GPRMC, 005251.000, V, 1732.8974, S, 14932.8328, W,, , 100619,, , N * 7
        #$GPGGA, 005252.000, 1732.8974, S, 14932.8328, W, 0, 00,, 204.2, M, 0.9, *1E
