from flask import Flask
from flask import render_template
from traiteGPS import traiteGPS
import subprocess
from lcd import main

app = Flask(__name__)

@app.route('/arduino/googlemap')
def arduino():
    cmd = "sudo dd if=/dev/ttyACM0 count=10"
    result = subprocess.check_output(cmd, shell=True)

    resultSTR = str(result).replace("\\r\\n", "\n")
    print (resultSTR)

    f = open("gpscoord.txt", "w")
    f.write(resultSTR)
    f.close()
    
    gps = traiteGPS("$GPRMC")
    return render_template('googlemap.html',resultat=gps)

@app.route('/arduino/streetmap')
def test_main():
    cmd = "sudo dd if=/dev/ttyACM0 count=10"
    result = subprocess.check_output(cmd, shell=True)

    resultSTR = str(result).replace("\\r\\n", "\n")
    #print (resultSTR)

    f = open("gpscoord.txt", "w")
    f.write(resultSTR)
    f.close()
		
    gps = traiteGPS("$GPRMC")
	#return render_template('arduino.html',resultat=gps)
    
	#affichage lcd    
    main(gps)
	#affichage page html
    return render_template('openstreet.html',resultat=gps)

@app.route('/user/<username>')
def show_user_profile(username):
    # show the user profile for that user
    return render_template('affichetonnom.html', name=username)


@app.route('/')
def hello_world():
    return 'Hello, World!'

