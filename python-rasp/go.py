from traiteGPS import traiteGPS

gps = traiteGPS("$GPRMC")
print("Latitude ", gps.getLatitude())
print("Longitude ", gps.getLongitude())

#le parseur automatique ne fonctionne pas avec $GPGGA
#gps = traiteGPS("$GPGGA")
#print("Latitude ", gps.getLatitude())
#print("Longitude ", gps.getLongitude())

